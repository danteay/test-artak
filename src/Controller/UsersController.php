<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersController extends BaseController
{
    /**
     * @Route("/users/login")
     * @Method()
     */
    public function login()
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                throw new \Exception("Invalid email format");
            }

            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['email' => $_POST['email']]);
            
            if (empty($user)) {
                throw new \Exception('No se encontro el usuario');
            }

            $hash = hash_hmac('sha256', $_POST['password'], $_ENV['HASH_KEY']);

            if ($hash != $user->getPass()) {
                throw new \Exception("Invalid password");
            }

            $_SESSION['user'] = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'lastname' =>$user->getLastname(),
                'email' => $user->getEmail()
            ];

            return new Response(json_encode([
                'status' => 'success',
                'message' => 'ok',
                'data' => $_SESSION['user']
            ]), 200, $contentType);
        } catch(\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $this->proccessError($e)
            ]), 500, $contentType);
        }
    }

    /**
     * @Route("/users/logout")
     * @Method()
     */
    public function logout()
    {
        unset($_SESSION['user']);
        return $this->redirect('/');
    }

    /**
     * @Route("/users")
     * @Method({"POST"})
     */
    public function create()
    {
        $contentType = ['Content-Type' => 'application/json'];

        if (empty($_POST['name'])) {
            return new Response(
                json_encode([
                    'status' => 'error',
                    'message' => 'Name can\'t be empty'
                ]),
                422,
                $contentType
            );
        }

        if (empty($_POST['lastname'])) {
            return new Response(
                json_encode([
                    'status' => 'error',
                    'message' => 'Lastame can\'t be empty'
                ]),
                422,
                $contentType
            );
        }

        if (empty($_POST['email'])) {
            return new Response(
                json_encode([
                    'status' => 'error',
                    'message' => 'Email can\'t be empty'
                ]),
                422,
                $contentType
            );
        }

        try {
            $user = new User();
            $user->setName($_POST['name']);
            $user->setLastname($_POST['lastname']);
            $user->setEmail($_POST['email']);
            $user->setPass($_POST['password']);

            $conx = $this->getDoctrine()->getManager();
            $conx->persist($user);
            $conx->flush();

            return new Response(json_encode([
                'status' => 'success',
                'message' => 'user created',
                'data' => [
                    'id' => $user->getId()
                ]
            ]), 200, $contentType);
        } catch(\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $this->proccessError($e)
            ]), 500, $contentType );
        }
    }
}
