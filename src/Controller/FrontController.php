<?php

namespace App\Controller;

use App\Entity\Contact;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PhpParser\Node\Expr\AssignOp\Concat;

class FrontController extends BaseController
{
    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function index()
    {
        if (isset($_SESSION) && array_key_exists('user', $_SESSION)) {
            return $this->redirect('/agenda');
        }
        return $this->render('users/login.twig', []);
    }

    /**
     * @Route("/register")
     * @Method({"GET"})
     */
    public function register()
    {
        if(isset($_SESSION) && array_key_exists('user', $_SESSION)) {
            return $this->redirect('/agenda');
        }
        return $this->render('users/register.twig',[]);
    }

    /**
     * @Route("/agenda")
     * @Method({"GET"})
     */
    public function agenda()
    {
        if(!array_key_exists('user', $_SESSION)) {
            return $this->redirect('/');
        }

        $cuser = $_SESSION['user'];

        $contacts = $this->getDoctrine()
            ->getRepository(Contact::class)
            ->findBy(['user_id' => $cuser['id']]);

        return $this->render('agenda.twig', [
            'contacts' => $contacts,
            'useremail' => $_SESSION['user']['email']
        ]);
    }

    /**
     * @Route("/contacts/new")
     * @Method({"GET"})
     */
    public function newContact()
    {
        if(!array_key_exists('user', $_SESSION)) {
            return $this->redirect('/');
        }

        $cuser = $_SESSION['user'];

        return $this->render('contacts/new.twig', [
            'userid' => $_SESSION['user']['id'],
            'useremail' => $_SESSION['user']['email']
        ]);
    }
}
