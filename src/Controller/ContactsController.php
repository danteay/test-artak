<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\SocialNetwork;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PhpParser\Node\Expr\AssignOp\Concat;

class ContactsController extends BaseController
{
    /**
     * @Route("/contacts/{id}")
     * @Method({"GET"})
     */
    public function show($id)
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (empty($id)) {
                throw new \Exception('Unexpected error');
            }

            $contact = $this->getDoctrine()
                ->getRepository(Contact::class)
                ->find($id);

            $networks = $this->getDoctrine()
                ->getRepository(SocialNetwork::class)
                ->findBy(['contact_id' => $contact->id]);

            if (empty($contact)) {
                throw new \Exception('Contact not found');
            }

            return $this->render('contacts/show.twig',[
                'contact' => $contact,
                'networks' => $networks,
                'useremail' => $_SESSION['user']['email']
            ]);
        } catch (\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]), 500, $contentType);
        }
    }

    /**
     * @Route("/contacts")
     * @Method({"POST"})
     */
    public function create()
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (empty($_POST['name'])) {
                throw new \Exception("Name can't be empty");
            }

            if (empty($_POST['phone'])) {
                throw new \Exception("Phone can't be empty");
            }

            if (empty($_POST['user_id'])) {
                throw new \Exception("Unexpected error, try again");
            }

            $contact = new Contact();
            $contact->name = $_POST['name'];
            $contact->lastname = $_POST['lastname'];
            $contact->email = $_POST['email'];
            $contact->phone = $_POST['phone'];
            $contact->address = $_POST['address'];
            $contact->user_id = $_POST['user_id'];

            $conx = $this->getDoctrine()->getManager();
            $conx->persist($contact);
            $conx->flush();

            return new Response(json_encode([
                'status' => 'success',
                'message' => 'ok',
                'data' => [
                    'id' => $contact->getId()
                ]
            ]), 200, $contentType);
        } catch(\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]), 500, $contentType);
        }
    }

    /**
     * @Route("/contacts/{id}/delete")
     * @Method({"GET"})
     */
    public function delete($id)
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (empty($id)) {
                throw new \Exception('Unexpected error');
            }

            $contact = $this->getDoctrine()
                ->getRepository(Contact::class)
                ->find($id);

            if (empty($contact)) {
                throw new \Exception('Contact not found');
            }

            $conx = $this->getDoctrine()->getManager();
            $conx->remove($contact);
            $conx->flush();

            return $this->redirect('/agenda');
        } catch(\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]), 500, $contentType);
        }
    }

    /**
     * @Route("/contacts/{id}/update")
     * @Method({"GET"})
     */
    public function viewUpdate($id)
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (empty($id)) {
                throw new \Exception('Unexpected error');
            }

            $contact = $this->getDoctrine()
                ->getRepository(Contact::class)
                ->find($id);

            if (empty($contact)) {
                throw new \Exception('Contact not found');
            }

            return $this->render('contacts/update.twig',[
                'contact' => $contact,
                'useremail' => $_SESSION['user']['email']
            ]);
        } catch (\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]), 500, $contentType);
        }
    }

    /**
     * @Route("/contacts/{id}/update")
     * @Method({"POST"})
     */
    public function update($id)
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (empty($id)) {
                throw new \Exception('Unexpected error');
            }

            $contact = new Contact();
            $contact = $this->getDoctrine()->getRepository(Contact::class)->find($id);

            if (empty($contact)) {
                throw new \Exception('Contact not found');
            }

            if (empty($_POST['name'])) {
                throw new \Exception("Name can't be empty");
            }

            if (empty($_POST['phone'])) {
                throw new \Exception("Phone can't be empty");
            }

            $contact->name = $_POST['name'];
            $contact->lastname = $_POST['lastname'];
            $contact->email = $_POST['email'];
            $contact->phone = $_POST['phone'];
            $contact->address = $_POST['address'];

            $conx = $this->getDoctrine()->getManager();
            $conx->flush();

            return new Response(json_encode([
                'status' => 'success',
                'message' => 'ok'
            ]), 200, $contentType);
        } catch (\Exception $e) {
            return new Response(json_encode([
                'status' => 'error',
                'message' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]), 500, $contentType);
        }
    }
}