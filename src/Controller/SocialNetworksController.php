<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\SocialNetwork;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PhpParser\Node\Expr\AssignOp\Concat;

class SocialNetworksController extends BaseController
{
    /**
     * @Route("/networks")
     * @Method({"POST"})
     */
    public function create()
    {
        if (empty($_POST['contact_id'])) {
            return $this->redirect('/agenda');
        }

        try {
            $networks = new SocialNetwork();
            $networks->network_type = $_POST['network_type'];
            $networks->setUrl($_POST['network_url']);
            $networks->contact_id = $_POST['contact_id'];

            $conx = $this->getDoctrine()->getManager();
            $conx->persist($networks);
            $conx->flush();

            return $this->redirect("/contacts/{$_POST['contact_id']}");
        } catch(\Exception $e) {
            return $this->redirect("/contacts/{$_POST['contact_id']}");
        }
    }

    /**
     * @Route("/networks/{id}/delete")
     * @Method({"GET"})
     */
    public function delete($id)
    {
        $contentType = ['Content-Type' => 'application/json'];

        try {
            if (empty($id)) {
                throw new \Exception('Unexpected error');
            }

            $network = $this->getDoctrine()
                ->getRepository(SocialNetwork::class)
                ->find($id);

            if (empty($network)) {
                throw new \Exception('Contact not found');
            }

            $contact_id = $network->contact_id;

            $conx = $this->getDoctrine()->getManager();
            $conx->remove($network);
            $conx->flush();

            return $this->redirect("/contacts/{$contact_id}");
        } catch(\Exception $e) {
            return $this->redirect("/contacts/{$contact_id}");
        }
    }
}