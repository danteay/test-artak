<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
     * Parse exception message and return a standard message
     * @param \Exception
     * @return string
     */
    protected function proccessError(\Exception $e)
    {
        if (preg_match('/SQLSTATE\[23000\]/', $e->getMessage())) {
            return "Email has been registed yet";
        } else {
            return $e->getMessage();
        }
    }

    /**
     * Validate if a session exists
     * @return Response
     */
    protected function withSession()
    {
        if(array_key_exists('user', $_SESSION)) {
            return $this->redirect('/agenda');
        }
    }

    /**
     * Validate if a session don't exists
     * @return Response
     */
    protected function withoutSession()
    {
        if(!array_key_exists('user', $_SESSION)) {
            return $this->redirect('/');
        }
    }
}