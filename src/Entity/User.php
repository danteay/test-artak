<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $pass;


    public function getId()
    {
        return $this->id;
    }   

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("Invalid email format");
        }

        $this->email = $email;
    }

    public function getPass()
    {
        return $this->pass;
    }   

    public function setPass($pass)
    {
        if (strlen($pass) < 8){
            throw new \Exception("Password nedds 8 characters as minimum");
        }

        $this->pass = hash_hmac('sha256', $pass, $_ENV['HASH_KEY']);
    }

}
