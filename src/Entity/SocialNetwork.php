<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocialNetworkRepository")
 */
class SocialNetwork
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    public $url;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('FACEBOOK', 'TWITTER', 'PINTEREST')", nullable=false)
     */
    public $network_type;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    public $contact_id;

    public function setUrl($url): self
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \Exception('Invalid Social Network URL');
        }

        $this->url = $url;
        return $this;
    }
}
