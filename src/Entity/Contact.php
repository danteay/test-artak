<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    public $phone;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    public $address;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    public $user_id;


    public function getFullname()
    {
        return $this->name . ' ' . $this->lastname;
    }
}
